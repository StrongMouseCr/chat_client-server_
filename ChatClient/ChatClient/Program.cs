﻿using System;
using System.Text;
using System.Net.Sockets;
using System.Threading;

namespace Client
{
    class Program
    {
        static TcpClient client;
        static NetworkStream stream;
        static string messageFromServer;
        static int positionX = 1;

        static void SendMessage()
        {
            string message = "";
            string nullstring = "";
            Console.SetCursorPosition(0, 0);
            Console.Write(">" + nullstring.PadLeft(Console.BufferWidth - 3, '_') + "<");
            Console.SetCursorPosition(1, 0);
            while (true)
            {
                ConsoleKeyInfo key = Console.ReadKey();
                positionX = Console.CursorLeft;
                if (key.Key == ConsoleKey.Enter)
                {
                    if (message.Length == 0)
                    {
                        Console.SetCursorPosition(0, 0);
                        Console.Write("> not sent" + nullstring.PadLeft(Console.BufferWidth - 12, '_') + "<");
                        Thread.Sleep(1000);
                        Console.SetCursorPosition(0, 0);
                        Console.Write(">" + nullstring.PadLeft(Console.BufferWidth - 3, '_') + "<");
                        Console.SetCursorPosition(1, 0);
                    }
                    else
                    {
                        if (message == "exit")
                            Disconnect();
                        else
                        {
                            byte[] data = Encoding.UTF8.GetBytes(message);
                            stream.Write(data);
                            Console.SetCursorPosition(0, 0);
                            Console.Write(">" + nullstring.PadLeft(Console.BufferWidth - 3, '_') + "<");
                            Console.SetCursorPosition(1, 0);
                            positionX = 1;
                            message = "";
                        }
                    }
                }
                else if(key.Key == ConsoleKey.Backspace)
                {
                    if (positionX == 0)
                    {
                        message += ">";
                        Console.SetCursorPosition(0, 0);
                        Console.Write(message + nullstring.PadLeft(Console.BufferWidth - 2 - message.Length, '_') + "<");
                        positionX++;
                        Console.SetCursorPosition(positionX, 0);
                        message = message.Remove(message.Length - 1);
                    }
                    else
                    {
                        Console.SetCursorPosition(0, 0);
                        message = message.Remove(message.Length - 1);
                        Console.Write(">" + message + nullstring.PadLeft(Console.BufferWidth - 2 - message.Length, '_') + "<");
                        Console.SetCursorPosition(positionX, 0);
                    }
                }
                else
                {
                    char symbol = key.KeyChar;
                    message += symbol.ToString();
                }
            }
        }

        static void ReceiveMessage()
        {
            int bufHeigh = 4;
            try
            {
                while (true)
                {
                    byte[] data = new byte[500000];
                    int i = stream.Read(data);
                    messageFromServer = Encoding.UTF8.GetString(data, 0, i);
                    Console.SetCursorPosition(0, bufHeigh);
                    bufHeigh++;
                    Console.Write("\n");
                    Console.WriteLine(messageFromServer);
                    Console.SetCursorPosition(positionX, 0);
                }
            }
            catch
            {
                Disconnect();
            }
        }

        static void Disconnect()
        {
            if (stream != null)
                stream.Close();//отключение потока
            if (client != null)
                client.Close();//отключение клиента
            Environment.Exit(0); //завершение процесса
        }

        static void Main(string[] args)
        {
            Console.Write("\nip>");
            string ip = Console.ReadLine();
            int port = 2006;
            Console.Write("your name>");
            string name = Console.ReadLine();

            client = new TcpClient(ip, port);
            stream = client.GetStream();

            byte[] data = Encoding.UTF8.GetBytes(name);
            stream.Write(data);

            Console.WriteLine(">connnect with chat<");
            Console.Write("(-_-;) 'exit' to disconnect (;-_-)");

            Thread receiveThread = new Thread(new ThreadStart(ReceiveMessage));
            receiveThread.Start();
            SendMessage();
        }
    }
}