﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace ChatServer
{
    public class ClientObject
    {
        public string id { get; set; }
        public NetworkStream stream { get; set; }
        string name;
        TcpClient client;
        ServerObject server;
        string path = @"D:\test\log.txt";

        public string GetName()
        {
            return name;
        }

        public ClientObject(TcpClient tcpClient, ServerObject serverObject)
        {
            id = Guid.NewGuid().ToString();
            client = tcpClient;
            server = serverObject;
            serverObject.AddToMembers(this);
        }

        public void Process()
        {
            try
            {
                stream = client.GetStream();
                string message = GetMessage();
                name = message;

                message = $">{name} join to chat<";
                server.messageToNotAll(message, this.id);
                Console.WriteLine(message);

                while (true)
                {
                    try
                    {
                        message = GetMessage();
                        string bufMessage = $"{name} >> {message}";
                        Console.WriteLine(bufMessage);
                        File.AppendAllText(path, bufMessage + "\n", Encoding.UTF8);
                        server.messageToAll(message, this.id);
                    }
                    catch
                    {
                        message = $">{name} left from chat<";
                        Console.WriteLine(message);
                        File.AppendAllText(path, message + "\n", Encoding.UTF8);
                        server.messageToNotAll(message, this.id);
                        break;
                    }
                }
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                server.RemoveConnection(this.id);
                Close();
            }
        }

        private string GetMessage()
        {
            byte[] data = new byte[500000];
            int i = stream.Read(data, 0, data.Length);
            string message = Encoding.UTF8.GetString(data, 0, i);
            return message;
        }

        public void Close()
        {
            if (stream != null)
                stream.Close();
            if (client != null)
                client.Close();
        }
    }


    public class ServerObject
    {
        TcpListener server;
        static string path = @"D:\test\log.txt";
        List<ClientObject> members = new List<ClientObject>();

        public void AddToMembers(ClientObject client)
        {
            members.Add(client);
        }

        public void Listen()
        {
            Console.Write("ip>");
            string ip = Console.ReadLine();
            IPAddress ipServer = IPAddress.Parse(ip);
            int port = 2004;

            server = new TcpListener(ipServer, port);
            server.Start();
            Console.WriteLine(">server start<");
            File.AppendAllText(path, $">server ::{ip}:: start<\n", Encoding.UTF8);
            while (true)
            {
                TcpClient client = server.AcceptTcpClient();
                ClientObject clientObject = new ClientObject(client, this);
                Thread clientThread = new Thread(new ThreadStart(clientObject.Process));
                clientThread.Start();
            }
        }

        public void messageToAll(string message, string id)
        {
            ClientObject clientOutput = members.FirstOrDefault(c => c.id == id);
            for (int i = 0; i < members.Count; i++)
            {
                if (members[i].id != id)
                {
                    string bufMessage = $"{clientOutput.GetName()} >> {message}";
                    byte[] data = Encoding.UTF8.GetBytes(bufMessage);
                    members[i].stream.Write(data, 0, data.Length);
                }
                else
                {
                    string bufMessage = $"you >> {message}";
                    byte[] data = Encoding.UTF8.GetBytes(bufMessage);
                    members[i].stream.Write(data, 0, data.Length);
                }
            }
        }

        public void messageToNotAll(string message, string id)
        {
            ClientObject clientOutput = members.FirstOrDefault(c => c.id == id);
            File.AppendAllText(path, message + "\n", Encoding.UTF8);
            byte[] data = Encoding.UTF8.GetBytes(message);
            for (int i = 0; i < members.Count; i++)
            {
                if (members[i].id != id)
                    members[i].stream.Write(data, 0, data.Length);
            }
        }

        public void RemoveConnection(string Id)
        {
            ClientObject clientRemove = members.FirstOrDefault(c => c.id == Id);
            if (clientRemove.id != null)
                members.Remove(clientRemove);
        }
    }


    class Program
    {
        static ServerObject server;
        static Thread thread;

        public static void Main(string[] args)
        {
            server = new ServerObject();
            thread = new Thread(new ThreadStart(server.Listen));
            thread.Start();
        }
    }
}